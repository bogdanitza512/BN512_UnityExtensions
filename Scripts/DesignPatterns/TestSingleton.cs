﻿using System.Collections;
using System.Collections.Generic;
using BN512.DesignPatterns;
using BN512.Extensions;
using UnityEngine;

public class TestSingleton : MonoSingleton<TestSingleton> {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    int i = 0;

    public int Shout()
    {
        i++;
        print("[{0}]: Ouch".FormatWith(i));
        if (i == 500)
        {
            Destroy(gameObject);
        }

        return i;

    }
}

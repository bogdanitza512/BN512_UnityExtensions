﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BN512.DesignPatterns;
using BN512.Extensions;

namespace BN512.DesignPatterns
{
	public class Toolbox : MonoSingleton<Toolbox>
    {
        protected Toolbox() { } // guarantee this will be always a singleton only - can't use the constructor!

        public float f;


        /// <summary>
        /// Registers the component.
        /// </summary>
        /// <returns>The component.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		static public T RegisterComponent<T>() where T : Component
        {
            return Instance.GetOrAddComponent<T>();
        }
    }
}


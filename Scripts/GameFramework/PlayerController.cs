﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using BN512.DesignPatterns;
using BN512.Extensions;

/// <summary>
/// Player Controller Class
/// </summary>
public class PlayerController : MonoBehaviour
{

    #region Fields and Properties

    /// <summary>
    /// The current game mode.
    /// </summary>
    public GameMode currentGameMode;

    [SerializeField]
    private KeyCode modifierKey = KeyCode.LeftShift;

    [SerializeField]
    private KeyCode restartKey = KeyCode.R;

    [SerializeField]
    private KeyCode exitKey = KeyCode.Q;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        if (currentGameMode == null)
            currentGameMode = FindObjectOfType<GameMode>();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(Input.GetKey(modifierKey) && Input.GetKey(restartKey))
        {
            currentGameMode.RestartScene();
        }

        if (Input.GetKey(modifierKey) && Input.GetKey(exitKey))
        {
            currentGameMode.QuitGame();
        }
    }

    #endregion

    #region Methods

	

    #endregion
}

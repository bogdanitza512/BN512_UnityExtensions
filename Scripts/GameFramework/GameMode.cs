﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using BN512.DesignPatterns;
using BN512.Extensions;
using Sirenix.OdinInspector;
using System.IO;
// using Sirenix.OdinInspector.Editor;
using Doozy.Engine.UI;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;

public enum SessionType { Quizz, Scoreboard, Quizz_Offline }

[System.Serializable]
public class ConfigData
{
    /// <summary>
    /// The target resolution.
    /// </summary>
    [SerializeField]
    [Tooltip("The target resolution.")]
    public Vector2Int targetResolution = new Vector2Int(1920, 1080);

    /// <summary>
    /// The target screen mode.
    /// </summary>
    [SerializeField]
    [Tooltip("The target screen mode.")]
    public FullScreenMode targetScreenMode = FullScreenMode.FullScreenWindow;

    /// <summary>
    /// Show debug view on start.
    /// </summary>
    [SerializeField]
    [Tooltip("Show debug view on start.")]
    public bool showDebugViewOnStart = true;

    /// <summary>
    /// The template in witch the ID is formated.
    /// </summary>
    [SerializeField]
    [Tooltip("The template in witch the ID is formated.")]
    public string IDTemplate = "{0}{1}{2}";

    /// <summary>
    /// The team number ID.
    /// </summary>
    [SerializeField]
    [Tooltip("The team number ID.")]
    public int teamID = 1;

    /// <summary>
    /// The type of session.
    /// </summary>
    [SerializeField]
    [Tooltip("The type of session.")]
    public SessionType sessionType = SessionType.Quizz;

    /// <summary>
    /// The sub team number ID.
    /// </summary>
    [SerializeField]
    [Tooltip("The target screen mode.")]
    public int subTeamID = 1;

    /// <summary>
    /// Editor URL to Database
    /// </summary>
    [SerializeField]
    [Tooltip("Editor URL to Database.")]
    public string editorDatabaseURL = "https://dd32-olumiant-quizz.firebaseio.com/";

    /// <summary>
    /// Score required to win.
    /// </summary>
    [SerializeField]
    [Tooltip("Score required to win.")]
    public int maxScore = 5;

    /// <summary>
    /// Returns session ID in string format
    /// </summary>
    [Button(ButtonSizes.Medium)]
    public string getSessionID()
    {
        string sessionID =
            IDTemplate.FormatWith(teamID,
                                  (int)sessionType,
                                  subTeamID);

        Debug.Log("Session ID: {0}".FormatWith(sessionID));

        return sessionID;
    }
}

[System.Serializable]
public class ConfigFile
{
    public string fileName = "config.json";

    [SerializeField]
    ConfigData data;

    bool isLoaded = false;

    public ConfigData Data
    {
        get
        {
            bool succes = true;
            if (!isLoaded) { succes = LoadConfigData(); }
            return succes ? data : null;
        }
    }

    public string filePath
    {
        get
        {
            return Path.Combine(Application.streamingAssetsPath, fileName);
        }
    }

    [Button(ButtonSizes.Medium)]
    public bool LoadConfigData()
    {
        if (File.Exists(filePath))
        {
            string dataAsJSON = File.ReadAllText(filePath);
            JsonUtility.FromJsonOverwrite(dataAsJSON, data);

            Debug.Log("[Config Loaded]:\n{0}".FormatWith(dataAsJSON));
            isLoaded = true;
            return true;
        }
        else
        {
            Debug.Log("Config file not found.");
            return false;
        }

    }

    [Button(ButtonSizes.Medium)]
    public bool SaveConfigData()
    {
        if (File.Exists(filePath))
        {
            string dataAsJSON = JsonUtility.ToJson(data, true);
            File.WriteAllText(filePath, dataAsJSON);

            Debug.Log("[Config Saved]:\n{0}".FormatWith(dataAsJSON));
            return true;
        }
        else
        {
            Debug.Log("Config file not found.");
            return false;
        }
    }
}

/// <summary>
/// 
/// </summary>
public class GameMode : MonoSingleton<GameMode>
{

    #region Fields and Properties

    public ConfigFile config;

    public FirebaseApp firebaseApp;
    public DatabaseReference databaseRoot;

    public bool isFirebaseReady = false;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        Screen.SetResolution(config.Data.targetResolution.x,
                             config.Data.targetResolution.y,
                             config.Data.targetScreenMode);
        if (config.Data.sessionType != SessionType.Quizz_Offline)
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    /**/
                    firebaseApp = FirebaseApp.DefaultInstance;
                    firebaseApp.SetEditorDatabaseUrl(config.Data.editorDatabaseURL);
                    databaseRoot = FirebaseDatabase.DefaultInstance.RootReference;
                    isFirebaseReady = true;
                    /**/
                }
                else
                {
                    Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                }
            });
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update() { /* TODO: No need for update */ }

    #endregion

    #region Methods

    /// <summary>
    /// Restarts the scene.
    /// </summary>
	public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        print("Unity.Editor::Playmode stopped by {0}".FormatWith(this.name));
#else
        Application.Quit();
#endif
    }

    #endregion
}
